# Casino Limit Challenge

![Casino Limit](./images/casinolimit.png)

## Context

**Casino Limit** is a pentest challenge created by the [PIRAT\\');](https://team.inria.fr/pirat/) research team in Rennes.
This challenge was proposed as a partner challenge by [Inria](https://inria.fr/) during the [Breizh CTF](https://www.breizhctf.com/) in 2024.
It was then deployed in the cloud in 120 different instances to enable each team to have its own version.
Virtual machines are automatically configured using the [URSID](https://gitlab.inria.fr/pirat-public/ursid) tool, a product of our ongoing research.

We now offer the possibility of replaying this challenge locally on your own machine, with a few limitations due to this local deployment. In particular, the initial machine is accessed via a direct IP rather than the domain name casinolimit.bzh.

And of course, no data will be logged on our servers.

This version has been pre-generated with [URSID](https://gitlab.inria.fr/pirat-public/ursid) to simplify its use and avoid installing too many dependencies.

Have fun!


## Requirements

To deploy locally **Casino Limit** you only need:
* Vagrant (>= 2.4.0)
* Ansible (>= 2.16)


## Deploy the challenge

You only have to clone this repository and do vagant up !

:warning: **intranet** VM may need some minutes to be really up because of docker building process.

```bash
$ git clone https://gitlab.inria.fr/pirat-public/casinolimit.git
$ cd casinolimit
$ vagrant up
```

If you want to stop the VM of the challenge:
```bash
$ cd casinolimit
$ vagrant halt
```

To restart VMs or after any reboot of your computer:
```bash
$ cd casinolimit
$ vagrant up
```

And when you want to clean up, you can remove the challenge with:
```bash
$ cd casinolimit
$ vagrant destroy
```


## Get ready to start !

The last step before starting the challenge is to get the IP of the initial machine.

As this is not a trivial matter with Vagrant, we've created a small script to perform the final actions required for the challenge to run smoothly, and to retrieve the startup url.

```bash
$ ./start_challenge.sh
```

Open the url from the output in your browser and enjoy !

## FAQ

### no IP displayed by start_challenge.sh

In case you have troubles with the script, it is probably an issue with the configuration of your private network in Virtualbox. We suppose that you have at least one private network distributing IP with a DHCP server. Check your configuration and activate the DHCP if disabled:

![Virtual Box private network](./images/vb_private_network.png)

Then, relaunch the script start_challenge.sh
