# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'open3'

# Version de l'API de Vagrantfile
API_VERSION = "2"

# Cluster Casino Limit
cluster = {
  "start" => 
  { 
    :ips => ["10.35.100.10"],
    :box => "debian/bookworm64",
    :group => "casinolimit",
    :cpus => 1,
    :mem => 1024,
    :networks => ["casino_network"],
    :playbook => "playbooks/main_playbook_start.yml",
  },
  "meetingcam" => 
  { 
    :ips => ["10.35.100.11"],
    :box => "debian/bookworm64",
    :group => "casinolimit",
    :cpus => 2,
    :mem => 2048,
    :networks => ["casino_network"],
    :playbook => "playbooks/main_playbook_meetingcam.yml",
  },
  "bastion" => 
  { 
    :ips => ["10.35.100.20", "10.135.100.20"],
    :box => "ubuntu/jammy64",
    :group => "casinolimit",
    :cpus => 2,
    :mem => 2048,
    :networks => ["casino_network", "intranet_network"],
    :playbook => "playbooks/main_playbook_bastion.yml",
  },
  "intranet" => 
  { 
    :ips => ["10.135.100.10"],
    :box => "ubuntu/jammy64",
    :group => "casinolimit",
    :cpus => 2,
    :mem => 2048,
    :networks => ["intranet_network"],
    :playbook => "playbooks/main_playbook_intranet.yml",
  }
}

Vagrant.configure(API_VERSION) do |config|
  config.vm.synced_folder ".", "/vagrant", disabled: true

  cluster.each_with_index do |(hostname, info), index|
    config.vm.define hostname do |srv|
      srv.vm.box = "#{info[:box]}"
      srv.vm.hostname = "#{hostname}"

      info[:networks].zip(info[:ips]).each do |network, ip|
        srv.vm.network "private_network", ip: "#{ip}", virtualbox__intnet: "#{network}"
      end

      # Start and Intranet have public IPs
      if srv.vm.hostname == "start" || srv.vm.hostname == "intranet" then
          srv.vm.network "private_network", type: "dhcp"
      end
      
      srv.vm.provision "ansible" do |ansible|
        ansible.compatibility_mode="2.0"
        ansible.playbook = "#{info[:playbook]}"
      #   ansible.extra_vars = {
      #   }                  
      end

      srv.vm.provider :virtualbox do |vb|
        vb.name = "#{hostname}"
        vb.customize ["modifyvm", :id, "--memory", info[:mem], "--cpus", info[:cpus], "--hwvirtex", "on"]
        vb.customize ["modifyvm", :id, "--groups", "/#{info[:group]}"]
      end
    end
  end
end
