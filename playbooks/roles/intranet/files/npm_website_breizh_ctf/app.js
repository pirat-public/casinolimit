var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var signupRouter = require('./routes/signup');
var profileRouter = require('./routes/profile');
var balancesRouter = require('./routes/balances');

var app = express();

// Global counter
global.counter = 0;
global.newfile = "";

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: "Da sicretu !", resave: false, saveUninitialized: false}));
app.use(express.static(path.join(__dirname, 'public')));

const public_paths = ["/login", "/signup", "/counter", "/newfile"]
app.use(function(req, res, next) {
  if (req.session.user) {
    console.log(`User: ${req.session.user.login}`);
    next();
  } else if (public_paths.includes(req.path)) {
    console.log("User: _guest");
    next();
  } else {
    res.redirect('/login');
  }
});

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/signup', signupRouter);
app.use('/profile', profileRouter);
app.use('/balances', balancesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  if (req.query.msg == undefined && err.status == 404) {
    res.redirect('/error?msg=If%20this%20page%20should%20exist,%20contact%20you%20administrator.');
  } else {
    // render the error page
    res.status(err.status || 500);
    res.render('error', {msg: req.query.msg});
  }
});

module.exports = app;
