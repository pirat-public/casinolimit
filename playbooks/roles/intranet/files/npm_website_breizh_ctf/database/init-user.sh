#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER intranet WITH PASSWORD 'T1mipLv2mm';
	GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE clients, balances TO intranet;
    GRANT SELECT, INSERT, UPDATE ON TABLE users TO intranet;
	GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO intranet;
EOSQL