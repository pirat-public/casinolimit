CREATE TYPE Status AS ENUM ('normal','premium','banned','watched');

CREATE TABLE users(
id SERIAL PRIMARY KEY,
login TEXT NOT NULL,
email TEXT,
password TEXT NOT NULL,
img TEXT
);

CREATE TABLE clients(
id SERIAL PRIMARY KEY,
given_name TEXT NOT NULL,
surname TEXT NOT NULL,
status Status
);

CREATE TABLE balances(
client SERIAL NOT NULL,
count REAL NOT NULL,
FOREIGN KEY (client) REFERENCES clients(id)
);

INSERT INTO users(login,email,password)
VALUES ('Eve','eve@casinolimit.bzh','$2b$10$FGRn.FGskzJBIQ0fv0JZvumYi15jUIFa.WkcI4ZcerilmTT1LX2XK'); --"J51gm!Jvc1m!44444"

INSERT INTO users(login,email,password)
VALUES ('le Chiffre','le.chiffre@casinolimit.bzh','$2b$10$Tfvu2VKGTUIRq7Nuzxk9SedxL90sPSFas8T08g2jyTPzk34cRaaWe'); --"Mjj4p;j5se1'ap,mevj51v"

INSERT INTO users(login,email,password)
VALUES ('Dimitrios','dimitrios@casinolimit.bzh','$2b$10$Ln8fdR71406nXireQcSBVOuGkin2Om9zLDH6uftxEXHD6NjTBqOqO'); --"fOSEF2c-1"

INSERT INTO users(login,email,password)
VALUES ('White','white@casinolimit.bzh','$2b$10$h3Ro83KQcTQfYHIIfuP7O.4pzPZro7op7c4Pa/WAhRvXmhoghzT6O'); --"Omg!Efc'e1lMdld?"

INSERT INTO users(login,email,password)
VALUES ('Gettler','gettler@casinolimit.bzh','$2b$10$rKz2ia6DDftF.afn5zeHKOvkvRDRSA31dT4.mTyGVELBeZ1/1beYi'); --"4gg!Li41tdM"

COPY clients(given_name, surname, status)
FROM '/docker-entrypoint-initdb.d/clients.csv'
DELIMITER ','
CSV HEADER;

COPY balances(client, count)
FROM '/docker-entrypoint-initdb.d/balances.csv'
DELIMITER ','
CSV HEADER;