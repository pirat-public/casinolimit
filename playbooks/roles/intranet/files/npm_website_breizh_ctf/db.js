const postgres = require('postgres');

const sql = postgres({
    host: process.env.PSQL_HOST,
    port: 5432,
    database: process.env.PSQL_DB,
    username: process.env.PSQL_UNAME,
    password: process.env.PSQL_PWD
});

module.exports = sql;