var express = require('express');
var router = express.Router();

const sql = require('../db.js');

router.get('/', async function(req, res, next) {
  var balances;
  try {
      balances = await sql`SELECT id, given_name, surname, status, count FROM clients INNER JOIN balances ON clients.id = balances.client ORDER BY id ASC`;
  } catch (error) {
      next(error);
  }
  res.render('balances', {balances});
});

router.get('/search', async function(req, res, next) {
  try {
    req.query.balances = await sql`SELECT id, given_name, surname, status, count FROM clients INNER JOIN balances ON clients.id = balances.client ORDER BY id ASC`;
  } catch (error) {
      next(error);
  }
  res.render('b_search', req.query);
  if ('q' in req.query) {
	  if (req.query['q'].includes("<%"))
	  {
	  counter++;
	  }
  }
});

module.exports = router;
