var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {});
});

router.get('/search', function(req, res, next) {
  res.render('index', req.query);
});

router.post('/logout', function(req, res, next) {
  res.clearCookie('connect.sid');
  req.session.destroy();
  res.redirect('/login');
});

router.get('/counter', function(req, res, next) {
  res.render('counter', {});
});

router.get('/newfile', function(req, res, next) {
  res.render('newfile', {});
});

module.exports = router;
