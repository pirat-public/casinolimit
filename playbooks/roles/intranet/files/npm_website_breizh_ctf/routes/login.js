const bcrypt = require('bcrypt');
var express = require('express');
var router = express.Router();

const sql = require('../db.js');


router.get('/', function(req, res, next) {
    res.render('login', {});
});


router.post('/', async function(req, res, next) {
    console.log("Post data: "+JSON.stringify(req.body));
    var users;
    try {
        users = await sql`SELECT * FROM users WHERE login=${req.body.login}`;
    } catch (error) {
        next(error);
    }
    if (users) {
        if (users.length == 0) {
            res.render('login', { wrong_login: true, login: req.body.login });
        } else {
            const user = users[0];
            bcrypt.compare(req.body.password, user.password).then(logged => {
                if (logged) {
                    req.session.user = user;
                    res.redirect('/');
                } else {
                    res.render('login', { wrong_pwd: true })
                }
            });
        }
    }
});

module.exports = router;
