var express = require('express');
var router = express.Router();
const multer = require('multer');

const sql = require('../db.js');

const multer_storage = multer.diskStorage({
    destination: './public/images',
    filename: (req, file, callback) => {
        console.log("Post data: "+JSON.stringify(req.body));
        var count = (req.body.title.match(/\.\./g) || []).length;
        var title = "";
        if (count == 0) {
            req.session.user.img = req.body.title;
        }
        if (count < 3) {
            title = req.body.title;
        }
        callback(null, `${title}`);
    }
});

const upload = multer({
    storage: multer_storage
}).single('file');


router.get('/', function(req, res, next) {
    user = req.session.user;
    res.render('profile', {user});
});

router.post('/', async function(req, res, next) {
    upload(req, res, async function (err) {
        const user = req.session.user;
        if (err || req.file == undefined) {
            res.render('profile', {user, err: true});
        } else {
            let render = true;
            if (user.img !== undefined) {
                try {
                    await sql`UPDATE users SET img=${user.img} WHERE id=${user.id}`;
                } catch (error) {
                    render = false;
                    next(error);
                }
            }
            if (render) {
                if (!req.file.path.includes("images"))
                {
                    newfile = "/app/" + req.file.path;
                }
                res.render('profile', {user, path: req.file.path});
            }
        }
    });
});


module.exports = router;
