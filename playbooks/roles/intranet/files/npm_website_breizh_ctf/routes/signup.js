const bcrypt = require('bcrypt');
var express = require('express');
var router = express.Router();

const sql = require('../db.js');

const nodemailer = require('nodemailer')
const transporter = nodemailer.createTransport({
    host: process.env.MAIL_IP,
    port: process.env.MAIL_PORT,
    secure: false,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PWD
    },
    tls: {
        rejectUnauthorized: false
    }
});

var user_count = 0;


router.get('/', function(req, res, next) {
    res.render('signup', {});
});


router.post('/', async function(req, res, next) {
    console.log("Post data: "+JSON.stringify(req.body));
    var message;
    var m_err;
    if (user_count < 6) {
        const login = req.body.login.substring(0,10);
        const password = (Math.random()+1).toString(36).substring(7); // generate random string of 5 char
        const hash = bcrypt.hashSync(password, 10);
        try {
            await sql`INSERT INTO users ${ sql({login: login, email: login+"@casinolimit.bzh", password: hash}, 'login', 'email', 'password')}`;
        } catch (error) {
            m_err = error;
        }
        user_count += 1;        
        message = {
            from: "intranet@casinolimit.bzh",
            to: process.env.MAIL_RCVER,
            subject: "Création de compte",
            text: `Cher administrateur,\n\nL'utilisateur:\n\nNom : ${login}\n\nvient de demander la création d'un compte sur l'intranet de casinolimit.bzh\nMot de passe: ${password}\n\nLe mot de passe de cet utilisateur a été généré automatiquement.\n\nVous voudrez bien communiquer ce mot de passe à l'utilisateur concerné lorsqu'il se présentera au poste de sécurité de l'accueil, et lui demander de le mettre à jour.\n`
        };
    } else if (user_count == 6) {
        user_count += 1;
        message = {
            from: process.env.MAIL_USER+"@casinolimit.bzh",
            to: process.env.MAIL_RCVER,
            subject: "[ALERT] Sign-up",
            text: "Too many requests to intranet sign-up. Function has been disabled to prevent denial of service."
        };
    }
    if (m_err != undefined) {
        next(m_err);
    } else {
        if (user_count < 7) {
            transporter.sendMail(message, (err, info) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log(info);
                }
            });
        }
        res.redirect('/login');
    }
  });

module.exports = router;
