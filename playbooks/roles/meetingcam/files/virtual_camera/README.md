This is a proof of concept for a API controlled camera system.

## Usage

Install the dependencies
```bash
pip3 install -r requirements.txt
```

Start the server
```bash
python3 ./src/main.py
```

## Ports

| Port | Default | Description |
| --- | --- | --- |
| HTTP_STREAM_PORT | 5000 | Port for the web server serving the video stream |
| TCP_CONTROL_PORT | 4242 | Port for the TCP camera control server |

## Options

These options are disabled by default.

| Option | Description |
| --- | --- |
| --stream | Enable the video stream, useful for debugging. Should work only with the virtual camera |
| --tcp | Enable the TCP control server |
| --virtual | Do not try to connect to the real camera, use the virtual camera instead |

## HTTP API

Replace `0.0.0.0` with the hostname of the server. Use `assets/aliases.sh` to create aliases for the commands.

```bash
# Take snapshot
curl -X GET http://0.0.0.0:5000/api/snapshot --output snapshot.jpg

# Move up
curl -X POST http://0.0.0.0:5000/api/move/up

# Move down
curl -X POST http://0.0.0.0:5000/api/move/down

# Move left
curl -X POST http://0.0.0.0:5000/api/move/left

# Move right
curl -X POST http://0.0.0.0:5000/api/move/right
```

## TODO
- Create ansible for thoses who dont want to use docker
- Implement tests with the API
- More tests on the camera backup system under heavy load
- Create the `pano.jpg` file of the meeting room


## Camera backup system

When the real camera is used, a virtual camera is also created in case of failure. 

The real camera is pinged every 10 seconds, and if it fails, the virtual camera is used instead.

The real camera can also fail if a command times out, or return a non zero status code.

When the real camera is back online, it is used again if it manages to take a snapshot successfully.