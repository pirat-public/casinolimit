# Take snapshot
alias snap="curl -X GET http://0.0.0.0:5000/api/snapshot --output snapshot.jpg"

# Move up
alias up="curl -X POST http://0.0.0.0:5000/api/move/up"

# Move down
alias down="curl -X POST http://0.0.0.0:5000/api/move/down"

# Move left
alias left="curl -X POST http://0.0.0.0:5000/api/move/left"

# Move right
alias right="curl -X POST http://0.0.0.0:5000/api/move/right"
