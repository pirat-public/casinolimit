# Take snapshot
alias rsnap="curl -X GET https://1778-193-54-192-15.ngrok-free.app/api/snapshot --output snapshot.jpg"

# Move up
alias rup="curl -X POST https://1778-193-54-192-15.ngrok-free.app/api/move/up"

# Move down
alias rdown="curl -X POST https://1778-193-54-192-15.ngrok-free.app/api/move/down"

# Move left
alias rleft="curl -X POST https://1778-193-54-192-15.ngrok-free.app/api/move/left"

# Move right
alias rright="curl -X POST https://1778-193-54-192-15.ngrok-free.app/api/move/right"
