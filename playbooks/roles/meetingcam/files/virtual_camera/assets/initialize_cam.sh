#!/bin/bash

# Enviroment variables
export $(grep -v '^#' /app/.env | xargs)

# Prepare the content of /bin/credentials.json
echo "{
  \"host\": \"${CAMERA_IP}:2020\",
  \"username\": \"Kasino\",
  \"password\": \"Kasino\"
}" > /bin/credentials.json

# Preparatory optionnal instructions (IPCam need to be online)
echo "Connecting to the camera at ${CAMERA_IP}"
/root/.pub-cache/bin/onvif --config-file /bin/credentials.json media1 get-profiles
/root/.pub-cache/bin/onvif --config-file /bin/credentials.json ptz get-presets --limit 3 --profile-token profile_1 | jq -r '.[] | ."@token" + "\t" + .Name'

# Warning, may fail if dart install package in another directory or multiple easy_onvif versions
cp /root/.pub-cache/global_packages/easy_onvif/bin/onvif.dart* /bin
chmod 755 /bin/onvif.dart*
chmod 744 /bin/credentials.json

ln -s /bin/onvif.dart* /bin/onvif

echo "alias onvif=\"dart run /bin/onvif --config-file /bin/credentials.json \"" >> /root/.bashrc
echo "alias snap=\"ffmpeg -y -loglevel fatal -rtsp_transport tcp -i rtsp://Kasino:Kasino@${CAMERA_IP}:554/stream1 -frames:v 1 -f image2 -r 1 -s 1920x1080 /root/snapshot.jpg \"" >> /root/.bashrc
echo "alias up=\"onvif ptz move --x=0.0 --y=0.1 -t profile_1\"" >> /root/.bashrc
echo "alias down=\"onvif ptz move --x=0.0 --y=-0.1 -t profile_1_\"" >> /root/.bashrc
echo "alias right=\"onvif ptz move --x=0.1 --y=0.0 -t profile_1\"" >> /root/.bashrc
echo "alias left=\"onvif ptz move --x=-0.1 --y=0.0 -t profile_1\"" >> /root/.bashrc

