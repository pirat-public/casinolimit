#!/bin/bash

# Take the url from the first parameter of the script and set the aliases to control the camera remotely
url=$1

echo 'alias snap="curl -X GET '$url'/api/snapshot --output snapshot.jpg"' >> .bash_aliases
echo 'alias up="curl -X POST '$url'/api/move/up"' >> .bash_aliases
echo 'alias down="curl -X POST '$url'/api/move/down"' >> .bash_aliases
echo 'alias left="curl -X POST '$url'/api/move/left"' >> .bash_aliases
echo 'alias right="curl -X POST '$url'/api/move/right"' >> .bash_aliases
