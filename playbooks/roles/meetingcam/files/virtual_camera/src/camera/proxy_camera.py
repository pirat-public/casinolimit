import requests
import time
import threading
import subprocess
import logging
import os

PROXY_CAMERA_HOST = os.getenv("PROXY_CAMERA_HOST", "4c180ba133cc38f7e1a907a71560daddf0026e5384d6d2247d6aa6b43633d5cd.bzh")
PROXY_CAMERA_PORT = os.getenv("PROXY_CAMERA_PORT", "5000")
NOT_FOUND_MESSAGE = os.getenv("NOT_FOUND_MESSAGE", "URL not found, please use the provided commands in the camera area instead of the API.")
HEALTHCHECK_SUCCESS_MESSAGE = os.getenv("HEALTHCHECK_SUCCESS_MESSAGE", "Camera operational.")
HEALTHCHECK_FAILURE_MESSAGE = os.getenv("HEALTHCHECK_FAILURE_MESSAGE", "Camera not operational.")

API_URL = "http://" + PROXY_CAMERA_HOST + ":" + PROXY_CAMERA_PORT

ROUTE_SNAPSHOT = API_URL + "/api/snapshot"
ROUTE_MOVE_UP = API_URL + "/api/move/up"
ROUTE_MOVE_DOWN = API_URL + "/api/move/down"
ROUTE_MOVE_LEFT = API_URL + "/api/move/left"
ROUTE_MOVE_RIGHT = API_URL + "/api/move/right"
ROUTE_RESET = API_URL + "/api/reset"
ROUTE_HEALTHCHECK = API_URL + "/api/healthcheck"

class ProxyCamera(threading.Thread):
    """
    Interface for the remote camera API.
    """

    def __init__(self):
        super().__init__()
        self.log = logging.getLogger("ProxyCamera")
        self.log.setLevel(logging.DEBUG)

        # Healthcheck
        self.timeout = 15
        self.healthcheck_interval = 10
        self.operational = None

        # First healthcheck
        self.log.info("Tying to connect to the remote camera API at " + API_URL)
        self.first_healthcheck()

        # Log status
        if self.operational:
            self.log.info("Connected to the remote camera API")
        else:
            self.log.warn("Could not connect to the remote camera API, is it on?")
        self.log.info("Proxy camera interface initialized")

    def run(self):
        self.running = True
        while self.running:
            self.healthcheck()
            time.sleep(self.healthcheck_interval)

    def get_jpeg(self):
        result = self.make_request(ROUTE_SNAPSHOT, "GET")
        if result is None:
            return None
        try:
            data = result.content
        except Exception as e:
            self.log.error(f"Failed to retrieve snapshot: {e}")
            self.operational = False
        return data
    
    def move_right(self):
        result = self.make_request(ROUTE_MOVE_RIGHT, "POST")
        self.log.debug("Made request to move right")
        return result.text
            
    def move_left(self):
        result = self.make_request(ROUTE_MOVE_LEFT, "POST")
        self.log.debug("Made request to move left")
        return result.text

    def move_up(self):
        result = self.make_request(ROUTE_MOVE_UP, "POST")
        self.log.debug("Made request to move up")
        return result.text

    def move_down(self):
        result = self.make_request(ROUTE_MOVE_DOWN, "POST")
        self.log.debug("Made request to move down")
        return result.text

    def reset(self):
        result = self.make_request(ROUTE_RESET, "POST")
        return result.text

    def toggle_light(self):
        self.log.warn("Remote camera does not support light control :(")
        pass

    def healthcheck(self):
        """
        Performs a healthcheck on the camera.
        A healthcheck consists of pinging the camera.
        If the camera was not operational, requires a successful snapshot to consider the camera operational again.
        """
        previous_operational = self.operational
        
        # Run the command and catch exceptions. If the command fails and the camera was operational, set operational to False
        result = None
        try:
            result = requests.get(ROUTE_HEALTHCHECK, timeout=self.timeout)
        except requests.exceptions.Timeout:
            if previous_operational == True:
                self.operational = False
                self.log.warn("Healthcheck timed out, remote camera API is not operational anymore")
        except Exception as e:
            if previous_operational == True:
                self.operational = False
                self.log.error(f"Healthcheck failed, remote camera API is not operational anymore: {e}")

        command_success = False
        if result is not None and HEALTHCHECK_SUCCESS_MESSAGE in result.text and result.status_code == 200:
            command_success = True
        
        # The command ran successfully, we still have to check the output
        if previous_operational:
            if not command_success:
                self.operational = False
                self.log.warn("Healthcheck failed, camera is not operational anymore")

        # If the camera was not operational, try to take a snapshot
        else:
            if command_success:
                self.log.debug("Camera responded, trying to take a snapshot")
                jpeg = self.get_jpeg()
                if jpeg is not None:
                    self.log.info("The remote camera API is back online!")
                    self.operational = True

    def first_healthcheck(self):
        """
        Performs the first healthcheck on the camera and attempt to connect to it.
        """
        try:
            result = requests.get(ROUTE_HEALTHCHECK, timeout=self.timeout)
        except requests.exceptions.Timeout:
            self.operational = False
            self.log.warn("First healthcheck timed out, remote camera API is not operational")
            return
        except Exception as e:
            self.operational = False
            self.log.error(f"First healthcheck failed, remote camera API is not operational: {e}")
            return
        
        if HEALTHCHECK_SUCCESS_MESSAGE in result.text and result.status_code == 200:
            self.operational = True
            self.log.info("First healthcheck successful, camera is operational")
        else:
            self.operational = False
            self.log.warn("First healthcheck failed, camera is not operational")
    
    def make_request(self, route, method):
        self.log.debug(f"Making request to {route} with method {method}")
        try:
            if method == "GET":
                result = requests.get(route, timeout=self.timeout)
            elif method == "POST":
                result = requests.post(route, timeout=self.timeout)
            else:
                raise ValueError("Invalid method")
        except requests.exceptions.Timeout:
            self.log.error("Request timed out")
            self.operational = False
            return None
        except Exception as e:
            self.log.error(f"Failed to make request: {e}")
            self.operational = False
            return None
        return result
        
    def stop(self):
        self.running = False
        self.operational = False
        self.log.info("Real camera interface stopped")
        if self.lock.locked():
            self.lock.release()


