import threading
import time
import subprocess
import logging
import os

CAMERA_IP = os.environ.get("CAMERA_IP", "192.168.42.42")

COMMAND_SNAP  = "ffmpeg -y -loglevel fatal -rtsp_transport tcp -i rtsp://Kasino:Kasino@"+CAMERA_IP+":554/stream1 -frames:v 1 -f image2 -r 1 -s 1920x1080 /root/snapshot.jpg"
COMMAND_RIGHT = "dart run /bin/onvif --config-file /bin/credentials.json ptz move --x=-0.1 --y=0.0 -t profile_1"
COMMAND_LEFT  = "dart run /bin/onvif --config-file /bin/credentials.json ptz move --x=0.1 --y=0.0 -t profile_1"
COMMAND_UP    = "dart run /bin/onvif --config-file /bin/credentials.json ptz move --x=0.0 --y=0.1 -t profile_1"
COMMAND_DOWN  = "dart run /bin/onvif --config-file /bin/credentials.json ptz move --x=0.0 --y=-0.1 -t profile_1"
COMMAND_RESET = "dart run /bin/onvif --config-file /bin/credentials.json ptz absolute-move --pan-tilt-x=0.0 --pan-tilt-y=0.0 -t profile_1"
SNAPSHOT_PATH = "/root/snapshot.jpg"
COMMAND_PING = "ping -c 1"
INIT_SCRIPT = ["./assets/initialize_cam.sh"]

class RealCamera(threading.Thread):
    """
    Interface for the real camera.
    """

    def __init__(self):
        super().__init__()
        self.log = logging.getLogger("RealCamera")
        self.log.setLevel(logging.DEBUG)

        # Healthcheck
        self.timeout = 15
        self.healthcheck_interval = 10
        self.operational = None

        # Initial position
        self.pos_x = 0.0
        self.pos_y = 0.0

        # Reset status
        self.has_reset = False

        # Mutex
        self.lock = threading.Lock()

        # First healthcheck
        self.log.info("Tying to connect to the real camera at " + CAMERA_IP)
        self.first_healthcheck()

        # Log status
        if self.operational:
            self.log.info("Connected to the real camera")
        else:
            self.log.warn("Could not connect to the real camera, is it on?")
    
        self.log.info("Real camera interface initialized")

    def run(self):
        self.running = True
        while self.running:
            if not self.has_reset and self.operational:
                self.reset()
                self.has_reset = True
            self.healthcheck()
            time.sleep(self.healthcheck_interval)

    def get_jpeg(self):
        self.lock.acquire()
        output = self.execute_command(COMMAND_SNAP)
        try:
            with open(SNAPSHOT_PATH, "rb") as image:
                data = image.read()
        except Exception as e:
            if self.operational:
                self.log.error(f"The snapshot was not saved correctly, camera os not operational anymore: {e}")
                self.operational = False
            data = None
        self.lock.release()
        return data
    
    def move_right(self):
        self.lock.acquire()
        if self.pos_x >= 0.9:
            self.log.warn("Cannot move right, already at the rightmost position")
            self.lock.release()
            return None
        self.pos_x += 0.1
        result = self.execute_command(COMMAND_RIGHT)
        self.log.debug("Moved right, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.lock.release()
        return result
            
    def move_left(self):
        self.lock.acquire()
        if self.pos_x <= -0.9:
            self.log.warn("Cannot move left, already at the leftmost position")
            self.lock.release()
            return None
        self.pos_x -= 0.1
        result = self.execute_command(COMMAND_LEFT)
        self.log.debug("Moved left, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.lock.release()
        return result

    def move_up(self):
        self.lock.acquire()
        if self.pos_y >= 0.9:
            self.log.warn("Cannot move up, already at the topmost position")
            self.lock.release()
            return None
        self.pos_y += 0.1
        result = self.execute_command(COMMAND_UP)
        self.log.debug("Moved up, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.lock.release()
        return result

    def move_down(self):
        self.lock.acquire()
        if self.pos_y <= -0.9:
            self.log.warn("Cannot move down, already at the bottommost position")
            self.lock.release()
            return None
        self.pos_y -= 0.1
        result = self.execute_command(COMMAND_DOWN)
        self.log.debug("Moved down, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.lock.release()
        return result

    def reset(self):
        self.lock.acquire()
        self.pos_x = 0.0
        self.pos_y = 0.0
        result = self.execute_command(COMMAND_RESET)
        self.log.debug("Reset position :(%d, %d)" % (self.pos_x, self.pos_y))
        self.lock.release()
        return result

    def toggle_light(self):
        self.log.warn("Real camera does not support light control :(")
        pass

    def healthcheck(self):
        """
        Performs a healthcheck on the camera.
        A healthcheck consists of pinging the camera.
        If the camera was not operational, requires a successful snapshot to consider the camera operational again.
        """
        previous_operational = self.operational
        
        # Run the command and catch exceptions. If the command fails and the camera was operational, set operational to False
        try:
            result = self.execute_command(COMMAND_PING + " " + CAMERA_IP, True) # Raises exception if ping fails
        except subprocess.TimeoutExpired:
            if previous_operational == True:
                self.operational = False
                self.log.warn("Healthcheck timed out, camera is not operational anymore")
        except Exception as e:
            if previous_operational == True:
                self.operational = False
                self.log.error(f"Healthcheck failed, camera is not operational anymore: {e}")
        
        # The command ran successfully, we still have to check the output
        if previous_operational:
            if result is None:
                self.operational = False
                self.log.warn("Healthcheck failed, camera is not operational anymore")

        # If the camera was not operational, try to take a snapshot
        else:
            if result is not None:
                self.log.debug("Ping successful, trying to take a snapshot")
                jpeg = self.get_jpeg()
                if jpeg is not None:
                    self.log.info("Real camera is back online!")
                    self.operational = True

    def first_healthcheck(self):
        """
        Performs the first healthcheck on the camera and attempt to connect to it.
        """
        try:
            result = self.execute_command(COMMAND_PING + " " + CAMERA_IP, True)
        except subprocess.TimeoutExpired:
            self.operational = False
            self.log.warn("First healthcheck timed out, camera is not operational")
            return
        except Exception as e:
            self.operational = False
            self.log.error(f"First healthcheck failed, camera is not operational: {e}")
            return
        
        if result is None:
            self.operational = False
            self.log.warn("First healthcheck failed, camera is not operational")
            return
        else:
            self.operational = True
        
    def execute_command(self, command:str, debug:bool=False)->bytes:
        self.log.debug("Executing " + str(command.split()))
        try:
            process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
            output, error = process.communicate(timeout=self.timeout)
            exit_code = process.wait()
        except subprocess.TimeoutExpired:
            process.kill()
            raise subprocess.TimeoutExpired(process.args, self.timeout)
        if error:
            if self.operational:
                self.log.error(f"Error executing command: {error}, camera is not operational anymore")
                self.operational = False
            return None
        if exit_code != 0:
            if self.operational:
                self.log.error("Command exited with code " + str(exit_code) + ", camera is not operational anymore")
                self.operational = False
            return None

        return output

    def stop(self):
        self.running = False
        self.operational = False
        self.log.info("Real camera interface stopped")
        if self.lock.locked():
            self.lock.release()


