import cv2
import threading
import numpy as np
import logging

LOCAL_FILENAME_LIGHT = "assets/pano.jpg"
LOCAL_FILENAME_DARK = "assets/pano.jpg"


class VirtualCamera():
    """
    Regular virtual camera.
    Handle movement, light control and snapshot.
    """

    def __init__(self, filename_light:str=LOCAL_FILENAME_LIGHT, filename_dark:str=LOCAL_FILENAME_DARK):

        self.jpeg = None
        self.filename_light = filename_light
        self.filename_dark = filename_dark
        self.full_image = cv2.imread(self.filename_light)
        self.dark_image = cv2.imread(self.filename_dark)
        self.height, self.width, _ = self.full_image.shape

        self.log = logging.getLogger("VirtualCamera")
        self.log.setLevel(logging.DEBUG)

        # Subframe parameters
        self.subframe_height=720
        self.subframe_width=1080

        self.subframe_step = self.height // 12

        # Initial position
        self.pos_y = 1500
        self.pos_x = 5950
        self.lights = True

        # Fix subframe position
        if self.pos_y + self.subframe_height > self.height:
            self.pos_y = self.height - self.subframe_height

        # Create initial jpeg
        self.update_jpg()

        # Mutex
        self.lock = threading.Lock()

        self.operational = True

        self.log.info("Initialized virtual camera")
        self.log.info("Source image size: {}x{}".format(self.width, self.height))
        self.log.info("Subframe size: {}x{}".format(self.subframe_width, self.subframe_height))

    def get_jpeg(self):
        return self.jpeg.tobytes()

    def move_right(self):
        self.lock.acquire()
        if self.pos_x >= self.width - self.subframe_width - self.subframe_step:
            self.log.warn("Cannot move right, already at the rightmost position")
            self.lock.release()
            return None
        self.pos_x += self.subframe_step
        self.log.debug("Moved right, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.update_jpg()
        self.lock.release()
        return True
            
    def move_left(self):
        self.lock.acquire()
        if self.pos_x <= self.subframe_step:
            self.log.warn("Cannot move left, already at the leftmost position")
            self.lock.release()
            return None
        self.pos_x -= self.subframe_step
        self.log.debug("Moved left, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.update_jpg()
        self.lock.release()
        return True

    def move_up(self):
        self.lock.acquire()
        if self.pos_y <= self.subframe_step:
            self.log.warn("Cannot move up, already at the topmost position")
            self.lock.release()
            return None
        self.pos_y -= self.subframe_step

        if self.pos_y < 0:
            self.pos_y = 0

        self.log.debug("Moved up, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.update_jpg()
        self.lock.release()
        return True

    def move_down(self):
        self.lock.acquire()
        if self.pos_y >= self.height - self.subframe_step:
            self.log.warn("Cannot move down, already at the bottommost position")
            self.lock.release()
            return None
        self.pos_y += self.subframe_step
        
        if self.pos_y + self.subframe_height > self.height:
            self.pos_y = self.height - self.subframe_height


        self.log.debug("Moved down, new position :(%f, %f)" % (self.pos_x, self.pos_y))
        self.update_jpg()
        self.lock.release()
        return True

    def reset(self):
        self.lock.acquire()
        self.pos_y = self.height//2
        self.pos_x = self.width//2
        self.log.debug("Reset position :(%d, %d)" % (self.pos_x, self.pos_y))
        self.update_jpg()
        self.lock.release()
        return True

    def toggle_light(self):
        self.lock.acquire()
        self.lights = not self.lights
        self.log.debug("Toggled light, new state: %s" % self.lights)
        self.update_jpg()
        self.lock.release()
        return True

    def healthcheck(self):
        pass

    def get_subframe(self, image, point, frame_height=426, frame_width=620):
        """
        Return a sub part of the image with horizontal looping
        """
        height, width, _ = image.shape
        x, y = point
        x = x % width

        if y < 0:
            y = 0

        if y + frame_height > height:
            y = height - frame_height
            
        subframe = image[y:y+frame_height, x:x+frame_width]
        if x + frame_width > width:
            subframe = np.concatenate((subframe, image[y:y+frame_height, :x+frame_width-width]), axis=1)
        return subframe
    
    def update_jpg(self):
        if self.lights:
            img = self.get_subframe(self.full_image, (self.pos_x, self.pos_y), self.subframe_height, self.subframe_width)
            _, img = cv2.imencode('.jpg', img)
            self.jpeg = img
        
        else:
            dark_img = self.get_subframe(self.dark_image, (self.pos_x, self.pos_y), self.subframe_height, self.subframe_width)
            _, dark_img = cv2.imencode('.jpg', dark_img)
            self.jpeg = dark_img



