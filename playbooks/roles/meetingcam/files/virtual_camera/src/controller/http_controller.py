import threading
import os
import logging
from flask import Flask, make_response
from werkzeug.serving import make_server
from camera import RealCamera, VirtualCamera

NOT_FOUND_MESSAGE = os.getenv("NOT_FOUND_MESSAGE", "URL not found, please use the provided commands in the camera area instead of the API.")
METHOD_NOT_ALLOWED_MESSAGE = os.getenv("METHOD_NOT_ALLOWED_MESSAGE", "Method not allowed.")
HEALTHCHECK_SUCCESS_MESSAGE = os.getenv("HEALTHCHECK_SUCCESS_MESSAGE", "Camera operational.")
HEALTHCHECK_FAILURE_MESSAGE = os.getenv("HEALTHCHECK_FAILURE_MESSAGE", "Camera not operational.")

def initialize_webserver(camera, backup_camera):
    """
    Create a Flask app and initialize the routes.

    Args:
        camera (RealCamera): the real camera
        backup_camera (VirtualCamera): the virtual camera
    """

    def chose_camera():
        if camera is not None and camera.operational:
            return camera
        else:
            return backup_camera

    app = Flask(__name__)

    @app.route('/api/move/<direction>', methods=['POST'])
    def move(direction):
        if direction == 'up':
            result = chose_camera().move_up()
        elif direction == 'down':
            result = chose_camera().move_down()
        elif direction == 'left':
            result = chose_camera().move_left()
        elif direction == 'right':
            result = chose_camera().move_right()
        else:
            return 'Invalid direction\n'
        if result is None:
            return 'Camera is already at the limit\n'
        else:
            return 'Camera moved ' + direction + '\n'
    
    @app.route('/api/snapshot', methods=['GET'])
    def snapshot():
        image_bytes = chose_camera().get_jpeg()
        if image_bytes is None:
            image_bytes = backup_camera.get_jpeg()
        response = make_response(image_bytes)
        response.headers.set('Content-Type', 'image/jpeg')
        response.headers.set('Content-Disposition', 'attachment', filename='snapshot.jpg')
        return response
    
    @app.route('/api/healthcheck', methods=['GET'])
    def healthcheck():
        if chose_camera().operational:
            return HEALTHCHECK_SUCCESS_MESSAGE + '\n', 200
        else:
            return HEALTHCHECK_FAILURE_MESSAGE + '\n', 503
    
    @app.route('/api/reset', methods=['POST'])
    def reset():
        chose_camera().reset()
        return 'Camera position reset\n'
    
    @app.errorhandler(404)
    def page_not_found(e):
        return NOT_FOUND_MESSAGE + "\n", 404
    
    @app.errorhandler(405)
    def method_not_allowed(e):
        return METHOD_NOT_ALLOWED_MESSAGE + "\n", 405

    
    return app

class HttpController(threading.Thread):

    def __init__(self, hostname:str, port:str, camera:RealCamera, backup_camera:VirtualCamera):
        """
        Create a new HTTP controller

        Args:
            hostname (str): the hostname to bind the server to
            port (str): the port to bind the server to
            camera (RealCamera): the real camera, can be None
            backup_camera (VirtualCamera): the virtual camera
        """
        threading.Thread.__init__(self,)
        self.hostname = hostname
        self.port = port
        self.camera = camera
        self.backup_camera = backup_camera

        self.log = logging.getLogger("HttpController")
        app = initialize_webserver(self.camera, self.backup_camera)
        self.webserver = make_server(self.hostname, self.port, app, threaded=True)

    def run(self):
        self.log.info(f"Serving API at http://{self.hostname}:{self.port}")
        self.webserver.serve_forever()

    def stop(self):
        self.webserver.shutdown()
        self.webserver = None
        self.camera = None