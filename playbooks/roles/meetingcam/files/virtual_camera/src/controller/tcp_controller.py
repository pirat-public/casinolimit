import threading
import socket
import base64
import logging

class TcpController(threading.Thread):
    """
    A simple TCP server to control the camera, only support the virtual camera for now
    """

    def __init__(self, hostname, port, camera):
        threading.Thread.__init__(self,)
        self.hostname = hostname
        self.port = port
        self.camera = camera
        self.log = logging.getLogger("TcpController")
        self.camera_mutex = threading.Lock()
        self.connexions = []
        self.running = False
        self.server = None
        self.timeout = 60*60


    def handle_connection(self, conn):
        """
        Serve a single connection

        Args:
            conn (socket): the connection object
        """
        serve = True
        conn.sendall("Welcome to the casino's IoT controller\nType help for more information\n".encode())
        prev_command = None
        while serve:
            try:
                data = conn.recv(1024)
                if not data:
                    raise Exception("Connection closed")

                if data == b"\n" and prev_command is not None:
                    data = prev_command

                prev_command = data

                # Lock the camera mutex to prevent the camera from being accessed by multiple threads at once
                self.camera_mutex.acquire()

                try:
                    if data == b"help\n":
                        conn.sendall("help - display this help message\nright - move camera right\nleft - move camera left\nup - move camera up\ndown - move camera down\nlight - toggle lights in the manager's office\nsnap - take snapshot\nquit - exit the program\n".encode())
                    elif data == b"right\n" or data == b"r\n":
                        self.camera.move_right()
                        conn.sendall("Camera moved right\n".encode())
                    elif data == b"left\n" or data == b"l\n":
                        self.camera.move_left()
                        conn.sendall("Camera moved left\n".encode())
                    elif data == b"up\n" or data == b"u\n":
                        self.camera.move_up()
                        conn.sendall("Camera moved up\n".encode())
                    elif data == b"down\n" or data == b"d\n":
                        self.camera.move_down()
                        conn.sendall("Camera moved down\n".encode())
                    elif data == b"snap\n":
                        jpg = self.camera.get_jpeg()
                        conn.sendall(base64.b64encode(jpg) + b"\n")
                    elif data == b"light\n" or data == b"lights\n":
                        self.camera.toggle_light()
                        conn.sendall("Lights toggled\n".encode())
                    elif data == b"quit\n" or data == b"exit\n":
                        serve = False
                    else:
                        conn.sendall("Unknown command\n".encode())

                except Exception as e:
                    self.camera_mutex.release()
                    raise e

                # Release the camera mutex
                self.camera_mutex.release()
            except Exception as e:
                self.log.error(f"Error handling connection: {e}")
                serve = False
                self.connexions.remove(conn)
                conn.close()
                


    def run(self):

        # Create a new server socket
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.server.bind((self.hostname, self.port))
        self.server.listen(1)
        self.running = True

        self.log.info(f"Controller listening on {self.hostname}:{self.port}")

        while self.running:
            conn, addr = self.server.accept()
            conn.settimeout(self.timeout)
            self.log.info(f"New TCP connection from {addr}")
            self.connexions.append(conn)

            thread = threading.Thread(target=self.handle_connection, args=(conn,), daemon=True)
            thread.start()
            
        self.log.info("TCP Controller stopped")

    def stop(self):
        self.running = False
        for conn in self.connexions:
            conn.shutdown()
        self.server.close()
    

