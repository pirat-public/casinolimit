import os
import argparse
import logging
import time
from dotenv import load_dotenv
load_dotenv()

from camera import RealCamera, VirtualCamera, ProxyCamera
from controller import TcpController, HttpController


CONTROLLER_PORT = int(os.environ.get('CONTROLLER_PORT', '4242'))
HTTP_PORT = int(os.environ.get('API_PORT', '5000'))
SERVER_HOSTNAME = os.environ.get('API_HOST', 'localhost')


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Start the camera API')
  parser.add_argument('--tcp', action='store_true', help='Enable TCP control capabilities')
  parser.add_argument('--virtual', action='store_true', help='Use the virtual camera only, do not attempt to connect to a real camera')
  parser.add_argument('--proxy', action='store_true', help='Use an external camera API instead of the real camera')
  args = parser.parse_args()

  logging.basicConfig(level=logging.INFO, format='[%(levelname)s] %(asctime)s - %(name)s - %(message)s')

  # Initialize the cameras
  real_camera = None
  if not args.virtual:
    if args.proxy:
      real_camera = ProxyCamera()
    else:
      real_camera = RealCamera()
    real_camera.start()
  virtual_camera = VirtualCamera()

  # Initialize the controllers
  http_server = HttpController(SERVER_HOSTNAME, HTTP_PORT, real_camera, virtual_camera)
  http_server.start()
  if args.tcp:
    tcp_server = TcpController(SERVER_HOSTNAME, CONTROLLER_PORT, virtual_camera)
    tcp_server.start()
    
  try:
    while True:
      time.sleep(1)

  except KeyboardInterrupt:
    if args.stream:
      virtual_camera.stop()
      virtual_camera.join()

    if args.tcp:
      tcp_server.stop()
      tcp_server.join()

    http_server.stop()
    http_server.join()

    real_camera.stop()
    real_camera.join()