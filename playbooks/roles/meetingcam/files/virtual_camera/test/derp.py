import requests
import os
import random

HOST = os.environ.get('API_HOST', 'localhost')
PORT = int(os.environ.get('API_PORT', '5000'))

sessions = [requests.Session() for _ in range(10)]
choices = ['up', 'down', 'left', 'right']

while True:
    s = random.choice(sessions)
    c = random.choice(choices)
    r = s.post(f"http://{HOST}:{PORT}/api/move/{c}")
