import socket
import os


HOST = "localhost"
PORT = int(os.environ.get('CONTROLLER_PORT', '4242'))

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while True:
    data = s.recv(1024)
    if not data:
        break
    s.sendall(b"right\n")