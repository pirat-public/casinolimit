cat casinolimit_banner

# Some IP 
IP_START=`vagrant ssh -c "hostname -I | cut -d' ' -f3" start 2>/dev/null`
IP_INTRANET=`vagrant ssh -c "hostname -I | cut -d' ' -f3" intranet 2>/dev/null`

# Setup intranet url in Bastion
# Must be done here because of Vagrant/Vbox local deploy
TOKEN=`vagrant ssh -c "sudo grep -rh http:// /home/admin/Mail | cut -d'/' -f3 | tr -d '\n'" bastion 2>/dev/null`
vagrant ssh -c "sudo grep -rl $TOKEN /home/admin/Mail | xargs sudo sed -ri 's/$TOKEN/$IP_INTRANET/g'" bastion 2>/dev/null

prefix="\033[1;32m"
suffix="\033[00m"

echo -e "$prefix"Welcome to Casino Limit Challenge !"$suffix"
echo "You can start here: http://$IP_START"
echo "Or directly ssh on the first machine as tbenedict user (pwd: NEi9g8Bc)"
echo "==>  ssh tbenedict@$IP_START"
echo ""